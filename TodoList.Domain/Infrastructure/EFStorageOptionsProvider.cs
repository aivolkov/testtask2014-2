﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.Domain.Infrastructure
{
    public class EFStorageOptionsProvider : IStorageOptionsProvider
    {
        private EFDbContext context;
        private Dictionary<Type, List<Expression>> joinLists = new Dictionary<Type, List<Expression>>();

        public EFStorageOptionsProvider(EFDbContext context)
        {
            this.context = context;
        }

        public void LoadWith<TEntity>(params System.Linq.Expressions.Expression<Func<TEntity, IEnumerable<IEntity>>>[] includeSettings)
            where TEntity : class, IEntity, new()
        {
            if (!joinLists.ContainsKey(typeof(TEntity)))
                joinLists.Add(typeof(TEntity), new List<Expression>());

            joinLists[typeof(TEntity)].AddRange(includeSettings);
        }


        public IQueryable<TEntity> Apply<TEntity>(DbSet<TEntity> entitiesSet)
            where TEntity : class, IEntity, new()
        {
            if (!joinLists.ContainsKey(typeof(TEntity)))
                return entitiesSet;

            IQueryable<TEntity> resultEntitiesSet = entitiesSet;
            foreach (var setting in joinLists[typeof(TEntity)])
            {
                resultEntitiesSet = resultEntitiesSet.Include(setting as Expression<Func<TEntity, IEnumerable<IEntity>>>);
            }
            return resultEntitiesSet;
        }
    }
}

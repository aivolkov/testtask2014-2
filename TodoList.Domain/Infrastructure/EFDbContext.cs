﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Domain.Entities;
using TodoList.Domain.Migrations;

namespace TodoList.Domain.Infrastructure
{
    public class EFDbContext : DbContext
    {
        static EFDbContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EFDbContext, Configuration>());
        }

        public DbSet<TodoItem> TodoItems { get; set; }
    }
}

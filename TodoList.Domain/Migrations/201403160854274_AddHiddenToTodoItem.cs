namespace TodoList.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHiddenToTodoItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TodoItems", "Hidden", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TodoItems", "Hidden");
        }
    }
}

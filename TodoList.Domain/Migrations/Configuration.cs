namespace TodoList.Domain.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TodoList.Domain.Entities;

    internal sealed class Configuration : DbMigrationsConfiguration<TodoList.Domain.Infrastructure.EFDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TodoList.Domain.Infrastructure.EFDbContext context)
        {
            context.TodoItems.AddOrUpdate(x => x.DueTime,
                    new TodoItem { Description = "�������� ������ �����", DueTime = new DateTime(2014, 03, 14, 17, 00, 01), FinishTime = new DateTime(2014, 03, 14, 10, 00, 00), Hidden = false },
                    new TodoItem { Description = "����������� ��������� �������", DueTime = new DateTime(2014, 03, 14, 17, 00, 02), FinishTime = new DateTime(2014, 03, 14, 12, 00, 00), Hidden = false },
                    new TodoItem { Description = "���������� ������", DueTime = new DateTime(2014, 03, 14, 17, 00, 03), FinishTime = null, Hidden = false },
                    new TodoItem { Description = "���������� ����� ������", DueTime = new DateTime(2014, 03, 14, 17, 00, 04), FinishTime = null, Hidden = false },
                    new TodoItem { Description = "����� �����", DueTime = new DateTime(2014, 03, 21, 17, 00, 05), FinishTime = null, Hidden = false },
                    new TodoItem { Description = "�������� ������", DueTime = new DateTime(2014, 03, 21, 17, 00, 06), FinishTime = null, Hidden = false },
                    new TodoItem { Description = "�������� ������ ����� ��������� ������", DueTime = new DateTime(2014, 03, 24, 17, 00, 07), FinishTime = null, Hidden = false },
                    new TodoItem { Description = "�������������� ������ �� ������ � ������", DueTime = new DateTime(2014, 03, 24, 17, 00, 08), FinishTime = null, Hidden = false }
                );
        }
    }
}

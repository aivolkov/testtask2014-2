﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.Domain.Infrastructure
{
    public class EFUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private EFDbContext context;

        public EFUnitOfWorkFactory(EFDbContext context)
        {
            this.context = context;
        }

        public IUnitOfWork GetUOW()
        {
            return new EFUnitOfWork(context);
        }
    }
}

﻿using Ninject;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TodoList.App.Services;
using TodoList.App.Services.Interfaces;
using TodoList.Domain.Infrastructure;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.Web
{
    public static class NinjectBindingConfiguration
    {
        public static void Register(IKernel kernel)
        {
            kernel.Bind<EFDbContext>().ToSelf().InRequestScope();
            kernel.Bind(typeof(IRepository<>)).To(typeof(EFRepository<>)).InRequestScope();
            kernel.Bind<IStorageOptionsProvider>().To<EFStorageOptionsProvider>().InRequestScope();
            kernel.Bind<IUnitOfWorkFactory>().To<EFUnitOfWorkFactory>().InRequestScope();
            kernel.Bind<IDateTimeService>().To<DateTimeService>().InSingletonScope();
        }
    }
}
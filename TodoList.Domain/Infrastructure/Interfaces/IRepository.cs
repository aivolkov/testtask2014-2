﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Domain.Infrastructure.Interfaces
{
    public interface IRepository<TEntity>
        where TEntity : IEntity
    {
        IQueryable<TEntity> GetAll();
        TEntity Get(int id);
        TEntity Add(TEntity entity);
        void Remove(TEntity entity);
        void RemoveByID(int ID);
    }
}

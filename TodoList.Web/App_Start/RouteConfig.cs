﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TodoList.Web.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("controllers", "{controller}/{action}");
            routes.MapRoute("default", "", new { controller = "Todo", action = "Index" });
        }
    }
}
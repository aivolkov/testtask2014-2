﻿using System;
using TodoList.App.Services.Interfaces;
using TodoList.Domain.Entities;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.App.TodoItems.Commands
{
    public class CheckTodoItem
    {
        private IRepository<TodoItem> todoItemsRepo;
        private IUnitOfWorkFactory uowFactory;
        
        public CheckTodoItem(IRepository<TodoItem> todoItemsRepo, IUnitOfWorkFactory uowFactory)
        {
            this.todoItemsRepo = todoItemsRepo;
            this.uowFactory = uowFactory;
        }

        public DateTime Execute(int id)
        {
			var finishTime = Service.Get<IDateTimeService>().Now;
			
            using (var uow = uowFactory.GetUOW())
            {
                var item = todoItemsRepo.Get(id);
                item.FinishTime = finishTime;

                uow.Commit();
            }
			
			return finishTime;
        }
    }
}

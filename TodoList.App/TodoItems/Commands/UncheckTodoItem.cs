﻿using TodoList.Domain.Entities;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.App.TodoItems.Commands
{
    public class UncheckTodoItem
    {
        private IRepository<TodoItem> todoItemsRepo;
        private IUnitOfWorkFactory uowFactory;

        public UncheckTodoItem(IRepository<TodoItem> todoItemsRepo, IUnitOfWorkFactory uowFactory)
        {
            this.todoItemsRepo = todoItemsRepo;
            this.uowFactory = uowFactory;
        }

        public void Execute(int id)
        {
            using (var uow = uowFactory.GetUOW())
            {
                var item = todoItemsRepo.Get(id);
                item.FinishTime = null;

                uow.Commit();
            }
        }
    }
}

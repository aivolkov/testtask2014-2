﻿window.EM = {
    classes: {
        view: {}
    },

    model: {
        dataReady: false,
        someData: undefined
    },
    
    views: {
        main: { }
    },
    
    initialize: function () {
        this.views.main = new EM.classes.view.Main();                

        // ======================= Events handlers ====================
        $(window).on('resize', function () {
            EM.views.main.windowResizeFixup();
        });
    },
    
    run: function () {
        this.initialize();
        EM.views.main.render();
    }
};

PL.classes.view.Main = Layout.Stuff.extend(function () {
    this.windowResizeFixup = function () { //Custom logic of view
        this.render();
    };
    
    this.beforeRenderFixup = function () {};
    this.afterRenderFixup = function () {};
    
    this.elementSelector = '.em-view-main';
    this.baseConstructor('view-main', '#em-view-main-template');
}, Layout.classes.RenderObj);

$(document).on('layout-ready', function () {
    EM.run();
});
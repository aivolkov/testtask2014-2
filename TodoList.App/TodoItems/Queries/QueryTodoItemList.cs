﻿using System.Collections.Generic;
using System.Linq;
using TodoList.App.TodoItems.Queries.Filters;
using TodoList.Domain.Entities;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.App.TodoItems.Queries
{
    public class QueryTodoItemList
    {
        private IRepository<TodoItem> todoItemsRepo;

        public QueryTodoItemList(IRepository<TodoItem> todoItemsRepo)
        {
            this.todoItemsRepo = todoItemsRepo;
        }

        public IEnumerable<TodoItem> Execute()
        {
            return Service.Get<FindActualTodoItems>().Execute(
                        todoItemsRepo.GetAll()
                    ).ToList();
        }
    }
}

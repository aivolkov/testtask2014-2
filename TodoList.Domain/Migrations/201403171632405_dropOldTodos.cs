namespace TodoList.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dropOldTodos : DbMigration
    {
        public override void Up()
        {
            this.Sql("delete from dbo.TodoItems");
        }
        
        public override void Down()
        {
        }
    }
}

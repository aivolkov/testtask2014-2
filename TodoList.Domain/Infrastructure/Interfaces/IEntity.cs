﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Domain.Infrastructure.Interfaces
{
    public interface IEntity
    {
        int ID { get; set; }
    }
}

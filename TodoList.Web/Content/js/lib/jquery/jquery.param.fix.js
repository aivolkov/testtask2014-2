﻿!function (jQuery) {
    var r20 = /%20/g;

    jQuery.param = function (a) {
        var s = [];

        // If an array was passed in, assume that it is an array of form elements.
        if (jQuery.isArray(a) || (a.jquery && !jQuery.isPlainObject(a))) {
            // Serialize the form elements
            jQuery.each(a, function () {
                add(this.name, this.value);
            });
        } else {
            // If traditional, encode the "old" way (the way 1.3.2 or older
            // did it), otherwise encode params recursively.
            for (var prefix in a) {
                buildParams(prefix, a[prefix], add);
            }
        }
        return s.join("&").replace(r20, "+");

        function add(key, value) {
            // If value is a function, invoke it and return its value
            value = jQuery.isFunction(value) ? value() : (value == null ? "" : value);
            s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value);
        };
    };

    function buildParams(prefix, obj, add) {
        var name;

        if (jQuery.isArray(obj)) {
            // Serialize array item.
            jQuery.each(obj, function (i, v) {
                buildParams(prefix + "[" + i + "]", v, add);
            });

        } else if (jQuery.type(obj) === "object") {
            // Serialize object item.
            for (name in obj) {
                buildParams(prefix + "." + name, obj[name], add);
            }
        } else {
            // Serialize scalar item.
            add(prefix, obj);
        }
    }
} (jQuery);
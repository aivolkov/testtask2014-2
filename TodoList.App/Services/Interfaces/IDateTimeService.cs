﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TodoList.App.Services.Interfaces
{
    public interface IDateTimeService
    {
        DateTime Now { get; }
    }
}

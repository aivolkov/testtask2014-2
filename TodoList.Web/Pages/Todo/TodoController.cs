﻿using System;
using System.Web.Mvc;
using TodoList.App.TodoItems.Queries;
using TodoList.Web.Pages.Todo.Models.ResponseModels;
using System.Linq;
using TodoList.App.TodoItems.Commands;
using TodoList.App.Services.Interfaces;

namespace TodoList.Web.Pages.Todo
{
    public class TodoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetData()
        {
            try
            {
                var todoItems = Service.Get<QueryTodoItemList>()
                    .Execute()
                    .Select(x => new TodoItem
                    {
                        id = x.ID,
                        description = x.Description,
                        dueTime = x.DueTime,
                        finishTime = x.FinishTime
                    })
                .ToList();

                return Json(new { status = true, data = todoItems }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Save(int? id, bool done, string text, DateTime date, int hours, int minutes)
        {
            try
            {
                // TODO: Реализовать метод сохранения

                return Json(new { status = true, data = id ?? new Random((int)Service.Get<IDateTimeService>().Now.Ticks).Next() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Check(int id)
        {
            try
            {
                var finishTime = Service.Get<CheckTodoItem>().Execute(id);

                return Json(new { status = true, data = finishTime }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Uncheck(int id)
        {
            try
            {
                Service.Get<UncheckTodoItem>().Execute(id);

                return Json(new { status = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}

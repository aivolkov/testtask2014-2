﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoList.App.Services.Interfaces;

namespace TodoList.App.Services
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}

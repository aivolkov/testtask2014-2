﻿using System.Linq;
using TodoList.Domain.Entities;

namespace TodoList.App.TodoItems.Queries.Filters
{
    public class FindActualTodoItems
    {
        public IQueryable<TodoItem> Execute(IQueryable<TodoItem> items)
        {
            return items.Where(x => !x.Hidden);
        }
    }
}

﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using TodoList.Web.App_Start;

namespace TodoList.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            ConfigureViewPathTemplates();
        }

        private void ConfigureViewPathTemplates()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine
            {
                ViewLocationFormats = new string[]
                { 
		            "~/Pages/{1}/[Views]/{0}.cshtml",		            
		            "~/Views/{0}.cshtml",
                },
                PartialViewLocationFormats = new string[]
                { 
		            "~/Pages/{1}/[Views]/{0}.cshtml"
                },
                MasterLocationFormats = new string[]
                {		            
		            "~/Views/{0}.cshtml"		            
                },
            });
        }
    }
}
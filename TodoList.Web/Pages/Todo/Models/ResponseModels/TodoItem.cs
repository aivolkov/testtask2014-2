﻿using System;

namespace TodoList.Web.Pages.Todo.Models.ResponseModels
{
    public class TodoItem
    {
        public int id;
        public string description;
        public DateTime dueTime;
        public DateTime? finishTime;
    }
}
﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList
{
    /// <summary>
    /// Service Locator
    /// </summary>
    public static class Service
    {
        private static IKernel _kernel;

        public static void SetKernel(IKernel kernel)
        {
            _kernel = kernel;
        }

        public static T Get<T>()
        {
            return _kernel.Get<T>();
        }
    }
}

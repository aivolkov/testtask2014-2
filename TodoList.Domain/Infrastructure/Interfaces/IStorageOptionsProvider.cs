﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TodoList.Domain.Infrastructure.Interfaces
{
    public interface IStorageOptionsProvider
    {
        void LoadWith<TEntity>(params Expression<Func<TEntity, IEnumerable<IEntity>>>[] includeSettings)
            where TEntity : class, IEntity, new();

        IQueryable<TEntity> Apply<TEntity>(DbSet<TEntity> dbSet)
            where TEntity : class, IEntity, new();
    }
}

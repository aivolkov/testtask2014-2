﻿window.TD = {
    classes: {
        view: {}
    },

    model: {
        dataReady: false,
        dataReceiveFailed: false,

        items: {},

        getItem: function (id) {
            if (!this._UUIDsMakingItem) this._UUIDsMakingItem = new TD.classes.Item();
            var indexUUID = this._UUIDsMakingItem.makeUUID(id);

            return this.items[indexUUID];
        }
    },

    views: {},
    
    queue: {},

    getData: function () {
        $.get(this.urls.getData)
            .done(function (response) {
                if (response.status) {
                    TD.model.dataReady = true;

                    for (var itemIndex in response.data) {
                        var receivedItem = response.data[itemIndex];
                        var anotherItem = new TD.classes.Item(receivedItem.id, receivedItem.description, moment(receivedItem.dueTime), receivedItem.finishTime === null ? null : moment(receivedItem.finishTime));
                        TD.model.items[anotherItem.makeUUID(anotherItem.id())] = anotherItem;
                    }

                    TD.views.main.initialize();
                } else {
                    TD.model.dataReceiveFailed = true;
                }

                TD.views.main.render();
            })
            .fail(function (xhr, msg) {
                console.log(msg);
                TD.views.main.render();
            });
    },

    initialize: function () {
        this.views.main = new TD.classes.view.Main();
        this.views.main.initialize();

        this.queue = new Layout.classes.QueueObj();
        
        var $document = $(document);

        // ======================= Events handlers ====================
        $(window).on('resize', function () {
            TD.views.main.windowResizeFixup();
        });

        $document.on('click', '.td-item-checkbox', function () {
            var $item = $(this).closest('.td-view-item'),
                itemId = $item.data('id');

            TD.views.main.todoList.getItemView(itemId).toggle();
        });
    },

    run: function () {
        this.initialize();
        TD.views.main.render();

        this.getData();
    }
};

TD.classes.view.Main = Layout.Stuff.extend(function () {
    this.todoList = new TD.classes.view.TodoList();

    this.initialize = function () {
        this.todoList.initialize();
    };

    this.windowResizeFixup = function () { //Custom logic of view
        this.render();
    };

    this.beforeRenderFixup = function () {
        this.todoList.beforeRenderFixup();
    };
    
    this.afterRenderFixup = function () {
        this.todoList.afterRenderFixup();
    };

    this.elementSelector = '.td-view-main';
    this.baseConstructor('view-main', '#td-view-main-template');
}, Layout.classes.RenderObj);

TD.classes.view.TodoList = Layout.Stuff.extend(function () {
    this.search = new TD.classes.view.Search();
    this.newItem = new TD.classes.view.NewItem();

    this._itemsViews = {};
    this.itemsViews = function () { return this._itemsViews; };

    this.getItemView = function (itemId) {
        if (!this._UUIDsMakingItemView) this._UUIDsMakingItemView = new TD.classes.view.Item();
        var indexUUID = this._UUIDsMakingItemView.makeUUID(itemId);

        return this.itemsViews()[indexUUID];
    };

    this.initialize = function () {
        this.search.initialize();
        this.newItem.initialize();

        for (var itemIndexUUID in TD.model.items) {
            var item = TD.model.items[itemIndexUUID];

            if (!this.getItemView(item.id())) {
                var anotherItemView = new TD.classes.view.Item(item.id());
                this.itemsViews()[anotherItemView.makeUUID(anotherItemView.itemId())] = anotherItemView;
            }
        }
    };

    this.beforeRenderFixup = function () {
        this.search.beforeRenderFixup();
        this.newItem.beforeRenderFixup();

        for (var itemViewIndexUUID in this.itemsViews()) {
            this.itemsViews()[itemViewIndexUUID].beforeRenderFixup();
        }

        this._previousTodoListItemsContainerScrollTop = $('.td-todo-list-items-container').scrollTop();
    };
    
    this.afterRenderFixup = function () {
        this.search.afterRenderFixup();
        this.newItem.afterRenderFixup();

        var $this = $('#' + this.uuid()),
            $itemsContainer = $this.find('.td-todo-list-items-container');

        if (!$this.length || !$itemsContainer.length) return;

        $itemsContainer.css('max-height', window.innerHeight - $itemsContainer[0].offsetTop - 10);

        for (var itemViewIndexUUID in this.itemsViews()) {
            this.itemsViews()[itemViewIndexUUID].afterRenderFixup();
        }

        $('.td-todo-list-items-container').scrollTop(this._previousTodoListItemsContainerScrollTop);
        delete this._previousTodoListItemsContainerScrollTop;
    };

    this.baseConstructor('view-todo-list', '#td-view-todo-list-template');
}, Layout.classes.RenderObj);

TD.classes.view.Search = Layout.Stuff.extend(function () {
    this._value = '';
    this.value = function () { return this._value; };

    this.initialize = function() { };

    this.windowResizeFixup = function () { //Custom logic of view
        this.render();
    };

    this.beforeRenderFixup = function () { };
    this.afterRenderFixup = function () { };

    this.baseConstructor('view-search', '#td-view-search-template');
}, Layout.classes.RenderObj);

TD.classes.view.NewItem = Layout.Stuff.extend(function () {
    this.initialize = function() { };

    this.windowResizeFixup = function () { //Custom logic of view
        this.render();
    };

    this.beforeRenderFixup = function () { };
    this.afterRenderFixup = function () { };

    this.baseConstructor('view-new-item', '#td-view-new-item-template');
}, Layout.classes.RenderObj);

TD.classes.view.Item = Layout.Stuff.extend(function (itemId) {
    this._itemId = itemId;
    this._processing = false;

    this.itemId = function () { return this._itemId; };
    this.processing = function (value) { if (value === undefined) return this._processing; this._processing = value; };

    this.item = function () { return TD.model.getItem(this.itemId()); };

    this.toggle = function () {
        var me = this;
        if (me.processing()) return;
        
        me.processing(true);
        me.render();

        me.item().toggle(function () {
            me.processing(false);
            me.render();
        }, function () {
            me.processing(false);
			me.render();
            Layout.UI.dangerMessage('Не удалось выполнить действие', 5000);
        });
    };

    this.windowResizeFixup = function () { //Custom logic of view
        this.render();
    };

    this.beforeRenderFixup = function () { };
    this.afterRenderFixup = function () { };

    this.baseConstructor('view-item', '#td-view-item-template');
}, Layout.classes.RenderObj);

TD.classes.Item = Layout.Stuff.extend(function (id, description, dueTime, finishTime) {
    this._id = id;
    this._description = description;
    this._dueTime = dueTime;
    this._finishTime = finishTime;

    this.id = function () { return this._id; };
    this.description = function (value) { if (value === undefined) return this._description; this._description = value; };
    this.dueTime = function (value) { if (value === undefined) return this._dueTime; this._dueTime = value; };
    this.finishTime = function (value) { if (value === undefined) return this._finishTime; this._finishTime = value; };

    this.done = function () { return this.finishTime() !== null; };

    this.toggle = function (successCallback, failCallback) {
        var me = this,
            check = me.finishTime() === null,
            url = check ? TD.urls.check : TD.urls.uncheck;

        var task = new Layout.classes.QueueTaskObj();
        task.run();

        task.run = function () {
            $.post(url, {
                id: me.id()
            }).done(function (response, xhr) {
                if (response.status) {
                    if (check) me.finishTime(moment(response.data));
                    else me.finishTime(null);

                    successCallback();
                    task.resolve();
                } else {
                    failCallback(xhr, response.message);
                    task.reject();
                }
            }).fail(function (xhr, msg) {
                failCallback(xhr, msg);
                task.reject();
            });

            return task;
        };

        TD.queue.push(task);
    };

    this.baseConstructor('item');
}, Layout.classes.UUIDObj);

$(document).on('layout-ready', function () {
    TD.run();
});
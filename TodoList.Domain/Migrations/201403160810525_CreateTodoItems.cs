namespace TodoList.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTodoItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TodoItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        DueTime = c.DateTime(nullable: false),
                        FinishTime = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TodoItems");
        }
    }
}

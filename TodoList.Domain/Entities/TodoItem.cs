﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.Domain.Entities
{
    public class TodoItem : IEntity
    {
        public int ID { get; set; }
        [Required]
        public string Description { get; set; }
        public DateTime DueTime { get; set; }
        public DateTime? FinishTime { get; set; }
        public bool Hidden { get; set; }
    }
}

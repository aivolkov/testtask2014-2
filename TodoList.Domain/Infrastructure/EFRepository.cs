﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.Domain.Infrastructure
{
    public class EFRepository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity, new()
    {
        protected readonly EFDbContext context;
        private IStorageOptionsProvider storageOptionsProvider;

        public EFRepository(EFDbContext context, IStorageOptionsProvider storageOptionsProvider)
        {
            this.context = context;
            this.storageOptionsProvider = storageOptionsProvider;
        }

        public IQueryable<TEntity> GetAll()
        {
            return storageOptionsProvider.Apply<TEntity>(context.Set<TEntity>());

        }

        public TEntity Get(int id)
        {
            return GetAll().Single(x => x.ID == id);
        }

        public TEntity Add(TEntity entity)
        {
            return context.Set<TEntity>().Add(entity);
        }

        public void Remove(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
        }

        public void RemoveByID(int ID)
        {
            var contextEntry = context.ChangeTracker.Entries<TEntity>().FirstOrDefault(x => x.Entity.ID == ID);
            if (contextEntry == null)
                context.Entry<TEntity>(new TEntity { ID = ID }).State = System.Data.Entity.EntityState.Deleted;
            else
                context.Entry<TEntity>(contextEntry.Entity).State = System.Data.Entity.EntityState.Deleted;
        }
    }
}

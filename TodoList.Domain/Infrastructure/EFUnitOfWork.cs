﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoList.Domain.Infrastructure.Interfaces;

namespace TodoList.Domain.Infrastructure
{
    public sealed class EFUnitOfWork : IUnitOfWork
    {
        private EFDbContext context;

        public EFUnitOfWork(EFDbContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            this.context = context;
        }

        public void Commit()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
        }
    }
}

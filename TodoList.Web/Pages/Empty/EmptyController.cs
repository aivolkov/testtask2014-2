﻿using System.Web.Mvc;

namespace TodoList.Web.Pages.Empty
{
    public class EmptyController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }        
    }
}
